import type CartItem from "./CartItem";

export default interface Cart{
    id? : number;
    total : number;
    recieved? : number;
    change? : number;
    payment? : string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    cartItems?: CartItem[];
}