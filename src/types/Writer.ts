export default interface Writer{
    id? : number;
    email : string;
    password : string;
    name : string;
    tel : string;
    bank_number : string;
    bank_account_name : string;
    date_of_registration : Date;
}