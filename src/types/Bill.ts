export default interface Bill{
    id? : number;
    qty : number;
    total_price : number;
    adminId : number;
    customerId : number;
    paymentId : number;
}