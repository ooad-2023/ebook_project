export default interface Payment{
    id? : number;
    date : Date;
    amount : number;
}