import type Ebook from "./Ebook";

export default interface Category{
    id : number;
    name : string;
    // ebook: Ebook[];
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}