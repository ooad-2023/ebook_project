export default interface Review{
    id? : number;
    rating : number;
    comment : string;
    ebookId : number;
    customerId : number;
}