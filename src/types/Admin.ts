export default interface Admin{
    id? : number;
    emailAd : string;
    password : string;
    name : string;
    date_of_registration : Date;
}