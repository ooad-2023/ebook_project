import type Cart from "./Cart";
import type Ebook from "./Ebook";

export default interface CartItem{
    id? : number;
    name : string;
    price : number;
    total : number;
    author : string;
    imageCover : string;
    imageFile : string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    cart?: Cart;
    ebook: Ebook;
}