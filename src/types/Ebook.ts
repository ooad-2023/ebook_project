import type Category from "./Category";
export default interface Ebook{
    id? : number;
    name? : string;
    author? : string;
    content? : string;
    price : number;
    uploaddate? : Date;
    imageCover? : string;
    imageFile? : string;
    categoryId? : number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    category? : Category;
}