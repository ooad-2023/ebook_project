import http from "./axios"

function getCustomers() {
    return http.get("/customers");
  }

  export default {getCustomers};