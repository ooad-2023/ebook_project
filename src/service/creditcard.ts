import type Customer from "@/types/Customer";
import http from "./axios"
import type Creditcard from "@/types/Creditcard";
import customer from "./customer";
function getCreditcards(params: any) {
  return http.get("/credits", {params: params});
}
function saveCreditcards(credit: Creditcard) {

  return http.post("/credits", credit);
}
function updateCreditcards(id:number, cred: Creditcard) {
  return http.patch(`"/credits"/${id}`, cred);
}

function delCreditcards(id:number) {
  return http.delete(`/credits/${id}`);
}
export default { getCreditcards,saveCreditcards,updateCreditcards, delCreditcards};