import http from './axios'

function getEbooks() {
  return http.get('/ebooks')
}

function getEbookById(id:number) {
  return http.get(`/ebooks/${id}`)
}

function getEbooksByCategory(id:number) {
  return http.get(`/ebooks/category/${id}`);
}
export default { getEbooks,getEbookById, getEbooksByCategory  };
