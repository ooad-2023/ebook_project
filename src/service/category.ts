import http from "./axios"

function getCategories() {
    return http.get("/categories");
  }

  export default {getCategories};
  