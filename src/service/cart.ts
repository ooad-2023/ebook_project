import http from './axios'

function getCarts() {
  return http.get('/carts')
}

function saveCarts(cart: {
  cartItems: { ebookId: number;}[]
}) {
  return http.post('/carts', cart)
}

function updateCarts() {
  return http.patch('/carts')
}

function delCarts(id: number) {
  return http.delete(`/carts/${id}`)
}
export default { getCarts, saveCarts, updateCarts, delCarts }
