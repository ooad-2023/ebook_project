import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: () => HomeView,
        header: () => import('@/components/headers/MenuHeader.vue')
      },
    },
    {
      path: '/cartlist',
      name: 'cartlist',
      components: {
        default: () => import('../views/cart/CartListView.vue'),
        header: () => import('@/components/headers/CartHeader.vue')
      },
    },
    {
      path: '/payment',
      name: 'payment',
      components: {
        default: () => import('../views/PmentView.vue'),
        header: () => import('@/components/headers/CartHeader.vue')
      },
    },
    {
      path: '/library',
      name: 'library',
      components: {
        default: () => import('../views/library/EbooklibraryView.vue'),
        header: () => import('@/components/headers/CartHeader.vue')
      },
    }
  ]
})

export default router
