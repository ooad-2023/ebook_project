import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import cartService from '@/service/cart'
import type Cart from '@/types/Cart'
import type Ebook from '@/types/Ebook'
import router from '@/router'
export const useCartStore = defineStore('cart', () => {
  const carts = ref<Cart[]>([])
  const cartItems = ref<{ ebook: Ebook; name: string; price: number; total: number; author: string; imageCover: string; imageFile: string; }[]>([])
  const cartList = ref<{ ebook: Ebook; qty: number; price: number; }[]>([])
  async function getCarts() {
    try {
      const res = await cartService.getCarts()
      carts.value = res.data
      console.log(carts.value)
    } catch (e) {
      console.log(e)
    }
  }

  function clearCarts() {
    cartList.value = []
  }

  function deleteEbook(index: number) {
    cartList.value.splice(index, 1)
  }

  async function addCart(item: Ebook) {
    for (let i = 0; i < cartList.value.length; i++) {
      if (cartList.value[i].ebook.id === item.id) {
        cartList.value[i].price = cartList.value[i].qty * item.price
        console.log(cartList.value)
        return
      }
    }
    cartList.value.push({ ebook: item, qty: 1, price: 1 * item.price })
  }
  const sumPrice = computed(() => {
    let sum = 0
    for (let i = 0; i < cartList.value.length; i++) {
      sum = sum + cartList.value[i].price
    }
    return sum
  })

  async function deleteCart(id: number) {
    try {
      const res = await cartService.delCarts(id)
      await getCarts()
    } catch (e) {
      console.log(e)
    }
  }

  async function openOrder() {
    const cartItems = cartList.value.map(
      (item) =>
        <{ ebookId: number;}>{
          ebookId: item.ebook.id,
        }
    )

    const cart = { cartItems: cartItems }
    console.log(cart)

    try {
      const res = await cartService.saveCarts(cart)
      carts.value = res.data
      console.log(carts.value)
      clearCarts()
    } catch (e) {
      console.log(e)
    }
  }

  function openPdf(cartItem: CartItem) {
    try {
      // ตรวจสอบว่า cartItem มีค่าและมี 'imageFile' ก่อนที่จะแสดง PDF
      if (cartItem && cartItem.imageFile) {
        const fileUrl = cartItem.imageFile;
  
        // สร้าง element <a> สำหรับเปิด PDF ในหน้าต่างใหม่
        const link = document.createElement('a');
        link.href = fileUrl;
        link.target = '_blank';
  
        // เพิ่ม element ไปยัง DOM และคลิกที่ลิงก์
        document.body.appendChild(link);
        link.click();
  
        // ลบ element ที่สร้างไว้
        document.body.removeChild(link);
      } else {
        console.error('ไม่สามารถเปิด PDF เนื่องจากข้อมูลไฟล์ไม่ถูกต้อง');
      }
    } catch (error) {
      console.error(error);
    }
  }
  
  // async function downloadPdf(cartItem: CartItem) {
  //   try {
  //     // ตรวจสอบว่า cartItem มีค่าและมี 'ebook.imageFile' ก่อนที่จะดาวน์โหลด
  //     if (cartItem && cartItem.imageFile) {
  //       const fileUrl = cartItem.imageFile;
  
  //       // ดาวน์โหลดไฟล์ PDF โดยให้มีชื่อไฟล์เป็นชื่อของ eBook
  //       const response = await fetch(fileUrl);
  //       const blob = await response.blob();
  
  //       // สร้าง URL สำหรับ Blob
  //       const blobUrl = URL.createObjectURL(blob);
  
  //       // สร้าง element <a> สำหรับเปิด PDF ในหน้าต่างใหม่
  //       const link = document.createElement('a');
  //       link.href = blobUrl;
  //       link.download = `${cartItem.imageFile}`; // ชื่อไฟล์ที่จะถูกดาวน์โหลด
  
  //       // เพิ่ม element ไปยัง DOM และคลิกที่ลิงก์
  //       document.body.appendChild(link);
  //       link.click();
  
  //       // ลบ element ที่สร้างไว้
  //       document.body.removeChild(link);
  
  //       // ลบ URL ที่ใช้สำหรับ Blob
  //       URL.revokeObjectURL(blobUrl);
  //     } else {
  //       console.error('ไม่สามารถดาวน์โหลด PDF เนื่องจากข้อมูลไฟล์ไม่ถูกต้อง');
  //     }
  //   } catch (error) {
  //     console.error(error);
  //   }
  // }

  return {
    carts,
    addCart,
    sumPrice,
    deleteEbook,
    cartList,
    getCarts,
    deleteCart,
    openOrder,
    clearCarts,
    openPdf
  }
})
