import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import creditService from "@/service/creditcard"
import type Customer from '@/types/Customer';
import type Creditcard from '@/types/Creditcard';

export const useCreditStore = defineStore('credit', () => {
  const dialog = ref(false);
  const credits = ref<Creditcard[]>([]);
  const editedCredit = ref<Creditcard>({ creditname: "",creditnumber: "",creditamount: 0 });


 

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedCredit.value= { creditname: "",creditnumber: "", creditamount: 0 };
    }
  });
  async function getCreditcards() {
  try {
    const res = await creditService.getCreditcards({});
    credits.value = res.data;
    console.log(res);
  } catch(e) {
    console.log(e);
  }
  }
  async function saveCredit() {
    try {
      const res = await creditService.saveCreditcards(editedCredit.value);
      dialog.value = false;
      await getCreditcards();
    } catch(e) {
      console.log(e);
    }
  }
  // async function deleteCustomer(id: number) {
  //   loadingStore.isLoading = true
  //   try {
  //     const res = await customerService.delCustomers(id);
  //     await getCustomers();
  //     successStore.showsuccess("ลบสำเร็จ")
  //   } catch(e) {
  //     console.log(e);
  //     messageStore.showError("ไม่สามารถลบข้อมูล Customer ได้")
  //   }
  //   loadingStore.isLoading = false
  // }

  function editCredit(credit: Creditcard) {
    editedCredit.value = JSON.parse(JSON.stringify(credit));;
    dialog.value = true;
  }

  return {
    dialog,
    credits,
    getCreditcards,
    editedCredit,
    saveCredit,
    editCredit,
  };
});

