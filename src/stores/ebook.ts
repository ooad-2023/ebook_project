import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import ebookService from '@/service/ebook'
import type Ebook from '@/types/Ebook'
export const useEbookStore = defineStore('ebook', () => {
  const dialog = ref(false);
  const ebooks = ref<Ebook[]>([]);
  const ebook =  ref<Ebook | null>(null);
  const editedEbook = ref<Ebook>({ name:"", author:"", content:"", price: 0, imageCover: "",categoryId:0})
  const ebookByCate = ref<Ebook[]>([]);
  console.log(ebookByCate)

  async function getEbooks() {
    try {
      const res = await ebookService.getEbooks()
      ebookByCate.value = res.data
      // console.log(ebooks.value)

      if (ebookByCate.value.length > 0) {
        ebook.value = ebookByCate.value[0];
      }
    } catch (e) {
      console.log(e)
    }
  }

  // async function getEbookById(id: number) {
  //   try {
  //     const res = await ebookService.getEbookById(id)
  //     ebooks.value = res.data
  //     console.log(ebooks.value)

  //     if (ebooks.value.length > 0) {
  //       ebook.value = ebooks.value[0];
  //     }
  //   } catch (e) {
  //     console.log(e)
  //   }
  // }

  async function getEbooksByCategory(id: number) {
    try {
      const res = await ebookService.getEbooksByCategory(id);
      ebookByCate.value = res.data;
      console.log(ebookByCate);
    } catch (e) {
      console.log(e);
    }
  }
 
  async function editEbook(ebook: Ebook) {
    editedEbook.value = ebook;
    dialog.value = true;
  }

  return { getEbooks, ebooks, dialog, ebook, ebookByCate,getEbooksByCategory,editedEbook,editEbook }
})
