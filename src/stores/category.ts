import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import categoryService from '@/service/category'
import type Category from '@/types/Category'
export const useCategoryStore = defineStore('category', () => {
  const categories = ref<Category[]>([])
  // const categories = ref([]);
  async function getCategories() {
    try {
      const res = await categoryService.getCategories()
      categories.value = res.data
      console.log(categories.value)
    } catch (e) {
      console.log(e)
    }
  }

  return { getCategories, categories }
})
