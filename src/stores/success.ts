import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useSuccessStore = defineStore('success', () => {
  const dialog = ref(false)
  
  return { dialog}
})
