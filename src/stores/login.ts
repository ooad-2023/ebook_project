import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoginStore = defineStore('login', () => {
  const loginEmail = ref("");
const isLogin = computed(() => {
  return loginEmail.value !== "";
})

const login = (email : string): void => {
  loginEmail.value = email;
  localStorage.setItem("loginEmail", email);
}
const logout = (): void => {
  loginEmail.value=""
  localStorage.removeItem("loginEmail");
}
const loadData = () => {
  loginEmail.value = localStorage.getItem("loginEmail") || "";
};

  return { loginEmail,isLogin,login,logout,loadData }
})
