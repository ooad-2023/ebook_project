import { ref } from 'vue'
import { defineStore } from 'pinia'
import type Customer from '@/types/Customer'

export const useCustomerStore = defineStore('customer', () => {
  const customers = ref<Customer[]>([{
    id : 1,email : "pat@gmail.com",
    password : "123456789",
    name : "Mod"
  }])
  const dialogLogin = ref(false)

  const login = (email: string, password: string): boolean => {
    const index = customers.value.findIndex((item) => item.name === email)
    if (index >= 0) {
      const customer = customers.value[index]
      if (customer.password === password) {
        return true
      }
      return false
    }
    return false
  }

  return { dialogLogin, login }
})
